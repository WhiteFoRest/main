global _start

section .text
_start:   mov     rax, 60          ; 'exit' syscall number
          mov     rdi, 0           ; rdi = 0
          syscall                  ; exit(0)
